package nl.powerhouse.consumption.dao;

import nl.powerhouse.consumption.domain.Fraction;
import nl.powerhouse.consumption.domain.MonthProfileKey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by elif on 27.11.2016.
 */
@Repository("fractionRepository")
@Transactional(readOnly = true)
public interface FractionRepository extends CrudRepository<Fraction, MonthProfileKey> {
    List<Fraction> findFractionByMonthProfileKeyProfile(@Param("profile") String profile);

    @Transactional
    void deleteByMonthProfileKeyProfile(@Param("profile") String profile);
}
