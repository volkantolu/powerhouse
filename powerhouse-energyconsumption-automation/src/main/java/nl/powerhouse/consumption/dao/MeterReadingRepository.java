package nl.powerhouse.consumption.dao;

import nl.powerhouse.consumption.domain.Fraction;
import nl.powerhouse.consumption.domain.MeterReading;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by elif on 27.11.2016.
 */
@Repository("meterReadingRepository")
@Transactional(readOnly = true)
public interface MeterReadingRepository extends CrudRepository<MeterReading, Long> {
    List<MeterReading> findMeterReadingByMonthProfileKeyProfile(@Param("profile") String profile);

    List<MeterReading> findMeterReadingByMonthProfileKeyMonth(@Param("month") String month);

    @Transactional
    void deleteByMonthProfileKeyProfile(@Param("profile") String profile);
}
