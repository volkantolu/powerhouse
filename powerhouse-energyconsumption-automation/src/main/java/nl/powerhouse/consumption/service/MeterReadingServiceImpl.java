package nl.powerhouse.consumption.service;

import nl.powerhouse.consumption.controller.helper.MeterReadingHelperImpl;
import nl.powerhouse.consumption.dao.FractionRepository;
import nl.powerhouse.consumption.dao.MeterReadingRepository;
import nl.powerhouse.consumption.domain.Fraction;
import nl.powerhouse.consumption.domain.MeterReading;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service("meterReadingService")
public class MeterReadingServiceImpl implements MeterReadingService {
    @Autowired
    private MeterReadingRepository meterReadingRepository;


    public List<MeterReading> getMeterReadingByProfile(String profile) {
        return meterReadingRepository.findMeterReadingByMonthProfileKeyProfile(profile);
    }

    public List<MeterReading> getAll() {
        List meterReadingList = new ArrayList();
        meterReadingRepository.findAll().forEach(meterReadingList::add);
        return meterReadingList;
    }
    public void deleteMeterReadingsByProfile(String profile){
        meterReadingRepository.deleteByMonthProfileKeyProfile(profile);
    }

    @Override
    public void addList(List<MeterReading> meterReadingList) {
        meterReadingRepository.save(meterReadingList);
    }

    @Override
    public Integer getConsumptionForAMonth(String month) {
        List<MeterReading> consumptions = meterReadingRepository.findMeterReadingByMonthProfileKeyMonth(month);
        int consumptionsValue = consumptions.stream().collect(Collectors.summingInt(m -> m.getMeterReading())).intValue();
        String prevMonth = MeterReadingHelperImpl.monthControlMap.get(month);
        int prevConsumptionsValue = 0;
        if (prevMonth != null) {
            List<MeterReading> prevConsumptions = meterReadingRepository.findMeterReadingByMonthProfileKeyMonth(prevMonth);
            prevConsumptionsValue = prevConsumptions.stream().collect(Collectors.summingInt(m -> m.getMeterReading())).intValue();
        }

        return consumptionsValue - prevConsumptionsValue;
    }
}
