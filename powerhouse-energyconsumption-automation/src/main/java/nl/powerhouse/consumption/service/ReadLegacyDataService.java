package nl.powerhouse.consumption.service;

/**
 * Created by elif on 27.11.2016.
 */
public interface ReadLegacyDataService {

    String readLegacyData();

}
