package nl.powerhouse.consumption.service;

import nl.powerhouse.consumption.dao.FractionRepository;
import nl.powerhouse.consumption.domain.Fraction;
import nl.powerhouse.consumption.domain.MonthProfileKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import nl.powerhouse.consumption.exception.*;
import java.util.ArrayList;
import java.util.List;


@Service("fractionService")
public class FractionServiceImpl implements FractionService {

    @Autowired
    private FractionRepository fractionRepository;


    public List<Fraction> getFractionByProfile(String profile) {
        return fractionRepository.findFractionByMonthProfileKeyProfile(profile);
    }

    public List<Fraction> getAll() {
        List fractionList = new ArrayList();
        fractionRepository.findAll().forEach(fractionList::add);
        return fractionList;
    }
    public void deleteFractionsByProfile(String profile){
        fractionRepository.deleteByMonthProfileKeyProfile(profile);
    }

    @Override
    public void addList(List<Fraction> fractionList) {
        fractionRepository.save(fractionList);
    }

}
