package nl.powerhouse.consumption.service;

import nl.powerhouse.consumption.domain.Fraction;

import java.util.List;

/**
 * Created by elif on 27.11.2016.
 */
public interface FractionService {

    List<Fraction> getFractionByProfile(String profile);

    List<Fraction> getAll();

    void deleteFractionsByProfile(String profile);

    void addList(List<Fraction> fractionList);

}
