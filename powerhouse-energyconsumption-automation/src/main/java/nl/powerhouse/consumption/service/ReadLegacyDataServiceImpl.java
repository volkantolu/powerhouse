package nl.powerhouse.consumption.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.powerhouse.consumption.domain.Fraction;
import nl.powerhouse.consumption.domain.MeterReading;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;

/**
 * Created by elif on 27.11.2016.
 */
@Service
public class ReadLegacyDataServiceImpl implements ReadLegacyDataService {

    private static final String LEGACY_FILE_PATH = "legacy-file.properties";

    private static final Logger logger = LoggerFactory.getLogger(ReadLegacyDataServiceImpl.class);

    @Autowired
    FractionService fractionService;

    @Autowired
    MeterReadingService meterReadingService;


    @Override
    public String readLegacyData() {
        String result = null;
        InputStream input = null;
        String meterReadingFile =  null;
        String fractionFile =  null;
        try {
            Properties prop = new Properties();
            URL url = getClass().getClassLoader().getResource(LEGACY_FILE_PATH);
            Path path = Paths.get(url.toURI());
            input = new FileInputStream(path.toFile());
            prop.load(input);

            meterReadingFile =  prop.getProperty("METERREADING_FILE_PATH");
            fractionFile =   prop.getProperty("FRACTION_FILE_PATH");

            ObjectMapper mapper = new ObjectMapper();

            List<Fraction> fractionList = mapper.readValue(new File(fractionFile), new TypeReference<List<Fraction>>(){});
            fractionService.addList(fractionList);

            List<MeterReading> meterReadingList = mapper.readValue(new File(meterReadingFile), new TypeReference<List<MeterReading>>(){});
            meterReadingService.addList(meterReadingList);

            result = "The legacy files are succesfully read";
        } catch (IOException e) {
            logger.error("Error reading legacy file", e);
            result = "Error reading legacy file";
        } catch (Exception e) {
            logger.error("Error reading legacy file", e);
            result = "Error reading legacy file";
        } finally {
            if (input != null) {
                try {
                    input.close();
                    new File(fractionFile).delete();
                    new File(meterReadingFile).delete();
                } catch (IOException e) {
                    logger.error("Error reading legacy file", e);
                    result = "Error reading legacy file";
                }  catch (Exception e) {
                    logger.error("Error reading legacy file", e);
                    result = "Error reading legacy file";
                }
            }
        }
        return result;
    }

}
