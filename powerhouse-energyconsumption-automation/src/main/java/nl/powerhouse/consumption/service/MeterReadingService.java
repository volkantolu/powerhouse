package nl.powerhouse.consumption.service;

import nl.powerhouse.consumption.domain.Fraction;
import nl.powerhouse.consumption.domain.MeterReading;

import java.util.List;

/**
 * Created by elif on 27.11.2016.
 */
public interface MeterReadingService {

    Integer getConsumptionForAMonth(String month);

    List<MeterReading> getMeterReadingByProfile(String profile);

    List<MeterReading> getAll();

    void addList(List<MeterReading> meterReadingList);

    void deleteMeterReadingsByProfile(String profile);
}
