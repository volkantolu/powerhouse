package nl.powerhouse.consumption.exception;

/**
 * Created by elif on 27.11.2016.
 */
public class EnergyConsumptionException extends RuntimeException {
    private String message;

    public EnergyConsumptionException(){
    }
    public EnergyConsumptionException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
