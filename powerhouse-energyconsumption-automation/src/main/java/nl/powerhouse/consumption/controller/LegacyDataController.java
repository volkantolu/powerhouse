package nl.powerhouse.consumption.controller;

import nl.powerhouse.consumption.domain.ReadLegacyDataResult;
import nl.powerhouse.consumption.service.ReadLegacyDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by elif on 27.11.2016.
 */
@Controller
@RequestMapping("/readlegacydata")
public class LegacyDataController {

    @Autowired
    ReadLegacyDataService readLegacyDataService;


    @RequestMapping(method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity readLegacyData() {
        try {
            String result = readLegacyDataService.readLegacyData();
            return new ResponseEntity<ReadLegacyDataResult>(new ReadLegacyDataResult(result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


}
