package nl.powerhouse.consumption.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import nl.powerhouse.consumption.domain.ConsumptionResult;
import nl.powerhouse.consumption.service.MeterReadingService;


@Controller
@RequestMapping("/consumption/test")
public class ConsumptionControllerTest {

    @Autowired
    private MeterReadingService meterReadingService;

    @RequestMapping(value = "/{month}", method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity getConsumptionByMonth(@PathVariable("month") String month) {
        try {
            Integer consumption = meterReadingService.getConsumptionForAMonth(month);
            return new ResponseEntity<ConsumptionResult>(new ConsumptionResult(consumption), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

}
