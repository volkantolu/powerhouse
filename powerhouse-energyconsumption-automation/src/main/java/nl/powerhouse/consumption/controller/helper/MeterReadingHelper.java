package nl.powerhouse.consumption.controller.helper;

import nl.powerhouse.consumption.domain.MeterReading;
import nl.powerhouse.consumption.exception.*;
import java.util.List;

/**
 * Created by elif on 27.11.2016.
 */

public interface MeterReadingHelper {

    String validate(List<MeterReading> fractionList) throws EnergyConsumptionException;
}
