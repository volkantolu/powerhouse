package nl.powerhouse.consumption.controller;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import nl.powerhouse.consumption.controller.helper.FractionHelper;
import nl.powerhouse.consumption.domain.Fraction;
import nl.powerhouse.consumption.exception.EnergyConsumptionException;
import nl.powerhouse.consumption.rest.client.RestClient;
import nl.powerhouse.consumption.service.FractionService;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Title("This is for testing fraction service")
@Features("CR 256 : Changing home screen template")
@Stories({"User can add fraction list"})
@Description("This is fraction test class ")
@Controller
@RequestMapping("/consumption/fraction/test")
public class FractionControllerTest {

    @Autowired
    private FractionService fractionService;

    @Autowired
    private FractionHelper fractionHelper;

    @Autowired
    public RestClient restClient;

    @RequestMapping(value = "/{profile}", method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity getFractionByProfile(@PathVariable("profile") String profile) {
        try {
            return new ResponseEntity<List<Fraction>>(fractionService.getFractionByProfile(profile), HttpStatus.OK);
        } catch (EnergyConsumptionException e) {
            return new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity list() {
        try {
            return new ResponseEntity<List<Fraction>>(fractionService.getAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }

    }

    @Test
    @Title("This will validate total sum of fractions for a given profile")
    @Step("Assertion for fraction")
    @Severity(SeverityLevel.CRITICAL)
    @Features("CR 256A :This is total value of fractions ")
    @Description("This is adding fraction test method")
    @Stories("User can add fraction list")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> addList(@RequestBody List<Fraction> fractionList) {
        try {
            fractionHelper.validateForTotalFraction(fractionList);
            fractionList.forEach(f -> {
                List<Fraction> oldFractionList = (List<Fraction>) getFractionByProfile(f.getMonthProfileKey().getProfile()).getBody();
                if (oldFractionList != null && oldFractionList.size() > 0)
                    throw new EnergyConsumptionException("Profile " + f.getMonthProfileKey().getProfile() + " already exists");
            });
			restClient.getRestCallForFraction("addList", "POST", fractionList);
            //fractionService.addList(fractionList);
            return new ResponseEntity<String>("Success", HttpStatus.OK);
        } catch (EnergyConsumptionException e) {
            return new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<String> update(@RequestBody List<Fraction> fractionList) {
        try {
            fractionHelper.validateForTotalFraction(fractionList);
            fractionList.forEach(f -> {
                List<Fraction> oldFractionList = (List<Fraction>) getFractionByProfile(f.getMonthProfileKey().getProfile()).getBody();
                if (oldFractionList == null || oldFractionList.size() == 0)
                    throw new EnergyConsumptionException(f.getMonthProfileKey().getProfile() + " profile does not exist");
            });
            //TODO:
            deleteByProfile(fractionList.get(0).getMonthProfileKey().getProfile());
            addList(fractionList);
            return new ResponseEntity<String>("Success", HttpStatus.OK);
        } catch (EnergyConsumptionException e) {
            return new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/{profile}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteByProfile(@PathVariable("profile") String profile) {
        try {
            fractionService.deleteFractionsByProfile(profile);
            return new ResponseEntity<String>("Success", HttpStatus.OK);
        } catch (EnergyConsumptionException e) {
            return new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }

}
