package nl.powerhouse.consumption.controller;

import nl.powerhouse.consumption.controller.helper.MeterReadingHelper;
import nl.powerhouse.consumption.domain.Fraction;
import nl.powerhouse.consumption.domain.MeterReading;
import nl.powerhouse.consumption.exception.*;
import nl.powerhouse.consumption.service.MeterReadingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by elif on 27.11.2016.
 */
@Controller
@RequestMapping("/consumption/meterreading")
public class MeterReadingController {
    @Autowired
    private MeterReadingService meterReadingService;

    @Autowired
    private MeterReadingHelper  meterReadingHelper;


    @RequestMapping(value = "/{profile}", method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity getMeterReadingByProfile(@PathVariable("profile") String profile) {
        try {
            return new ResponseEntity<List<MeterReading>>(meterReadingService.getMeterReadingByProfile(profile), HttpStatus.OK);
        } catch (EnergyConsumptionException e) {
            return new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity list() {
        try {
            return new ResponseEntity<List<MeterReading>>(meterReadingService.getAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> addList(@RequestBody List<MeterReading> meterReadingList) {
        String errorMsg = null;
        try {
            errorMsg =  meterReadingHelper.validate(meterReadingList);
            meterReadingList.forEach(f -> {
                List<MeterReading> oldMeterReadingList = (List<MeterReading>) getMeterReadingByProfile(f.getMonthProfileKey().getProfile()).getBody();
                if (oldMeterReadingList != null && oldMeterReadingList.size() > 0)
                    throw new EnergyConsumptionException("Meter Reading for " + f.getMonthProfileKey().getProfile() + " already exists");
            });
            if (errorMsg == null || errorMsg.length() == 0) {
                meterReadingService.addList(meterReadingList);
                return new ResponseEntity<String>("Success", HttpStatus.OK);
            } else {
                return new ResponseEntity<String>(errorMsg, HttpStatus.OK);
            }
        } catch (EnergyConsumptionException e) {
            return new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<String> update(@RequestBody List<MeterReading> meterReadingList) {
        try {
            meterReadingHelper.validate(meterReadingList);
            meterReadingList.forEach(f -> {
                List<MeterReading> oldMeterReadingList = (List<MeterReading>) getMeterReadingByProfile(f.getMonthProfileKey().getProfile()).getBody();
                if (oldMeterReadingList == null || oldMeterReadingList.size() == 0)
                    throw new EnergyConsumptionException("Meter reading for " + f.getMonthProfileKey().getProfile() + " profile does not exist");
            });
            deleteByProfile(meterReadingList.get(0).getMonthProfileKey().getProfile());
            addList(meterReadingList);
            return new ResponseEntity<String>("Success", HttpStatus.OK);
        } catch (EnergyConsumptionException e) {
            return new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/{profile}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteByProfile(@PathVariable("profile") String profile) {
        try {
            meterReadingService.deleteMeterReadingsByProfile(profile);
            return new ResponseEntity<String>("Success", HttpStatus.OK);
        } catch (EnergyConsumptionException e) {
            return new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }


}
