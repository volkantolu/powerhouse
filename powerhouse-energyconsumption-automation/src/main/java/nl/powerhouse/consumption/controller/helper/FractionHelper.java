package nl.powerhouse.consumption.controller.helper;

import nl.powerhouse.consumption.domain.Fraction;
import nl.powerhouse.consumption.exception.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by elif on 27.11.2016.
 */

public interface FractionHelper {

	void validateForTotalFraction(List<Fraction> fractionList) throws EnergyConsumptionException;
}
