package nl.powerhouse.consumption.controller.helper;

import nl.powerhouse.consumption.domain.Fraction;
import nl.powerhouse.consumption.domain.MeterReading;
import nl.powerhouse.consumption.exception.*;
import nl.powerhouse.consumption.service.FractionService;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.constraints.AssertTrue;


@Component
public class MeterReadingHelperImpl implements MeterReadingHelper {

    @Autowired
    private FractionService fractionService;

    public static Map<String, String> monthControlMap = new HashMap<>();

    static {
        monthControlMap.put("FEB", "JAN");
        monthControlMap.put("MAR", "FEB");
        monthControlMap.put("APR", "MAR");
        monthControlMap.put("MAY", "APR");
        monthControlMap.put("JUN", "MAY");
        monthControlMap.put("JUL", "JUN");
        monthControlMap.put("AUG", "JUL");
        monthControlMap.put("SEP", "AUG");
        monthControlMap.put("OCT", "SEP");
        monthControlMap.put("NOV", "OCT");
        monthControlMap.put("DEC", "NOV");
    }

    @Override
    public String validate(List<MeterReading> meterReadingList) throws EnergyConsumptionException {
        StringBuilder errorMsg = new StringBuilder();
        if (meterReadingList == null || meterReadingList.size() == 0)
            throw new EnergyConsumptionException("Validation error : MeterReading list cannot be empty");

        Map<String, IntSummaryStatistics> profileMap =
                meterReadingList.stream().collect(Collectors.groupingBy(m -> m.getMonthProfileKey().getProfile(), Collectors.summarizingInt(MeterReading::getMeterReading)));

        Set<String> profiles = profileMap.keySet();

        if (profiles == null || profiles.size() == 0) {
            Assert.assertFalse("Validation error : No profile found", profiles == null || profiles.size() == 0);
        } else {

            profiles.forEach(profile -> {
                if (profileMap.get(profile).getCount() != 12) {
                    Assert.assertEquals("Validation error : Profile " + profile + " must have 12 months", 12, profileMap.get(profile).getCount());
                }

                List<Fraction> fractionList = fractionService.getFractionByProfile(profile);

                if (fractionList == null || fractionList.size() == 0) {
                    Assert.assertFalse("There is no data for profile " + profile, fractionList == null || fractionList.size() == 0);
                }

                List<MeterReading> tmpMeterReadingList = meterReadingList.stream().filter(m -> m.getMonthProfileKey().getProfile().equals(profile)).collect(Collectors.toList());
                long totalConsumptionForAYear = profileMap.get(profile).getSum();

                for (MeterReading meterReading : tmpMeterReadingList) {
                    String currMonth = meterReading.getMonthProfileKey().getMonth();
                    String prevMonth = monthControlMap.get(currMonth);
                    //For Jan prev month reading is zero
                    int prevMonthMeterReadingValue = 0;
                    if (prevMonth != null) {
                        Stream<MeterReading> prevMonthStream = tmpMeterReadingList.stream().filter(m -> m.getMonthProfileKey().getMonth().equals(prevMonth));
                        MeterReading prevMonthMeterReading = prevMonthStream.collect(Collectors.toList()).get(0);
                        prevMonthMeterReadingValue = prevMonthMeterReading.getMeterReading();
                    }

                    if (prevMonth != null && meterReading.getMeterReading() < prevMonthMeterReadingValue) {
                    	
                    	Assert.assertFalse("Validation error : " + currMonth + " consumption value cannot " +
                                "be lower than " + prevMonth + " for profile " + meterReading.getMonthProfileKey().getProfile(),(prevMonth != null && meterReading.getMeterReading() < prevMonthMeterReadingValue));
                        break;
                    }

                    List<Fraction> list = fractionList.stream().filter(f -> f.getMonthProfileKey().getMonth().equals(currMonth)).collect(Collectors.toList());

                    if (list == null || list.size() == 0) {
                    	Assert.assertFalse("Validation error : " + " No fraction found for profile " + profile, list == null || list.size() == 0);
                        break;
                    } else {
                        Fraction fractionForCurrentMonth = list.get(0);
                        double expectedConsumption = totalConsumptionForAYear * fractionForCurrentMonth.getFraction();
                        int consumption = meterReading.getMeterReading() - prevMonthMeterReadingValue;
                        double lowerLimit = expectedConsumption - expectedConsumption * 0.25;
                        double higherLimit = expectedConsumption + expectedConsumption * 0.25;
                        if (consumption < lowerLimit || consumption > higherLimit) {
                        	Assert.assertFalse("Validation error : " + " Expected consumption for profile " + profile + " and month " + currMonth + " is between " + lowerLimit + "-" + higherLimit + " but current consumption is " + consumption,  consumption < lowerLimit || consumption > higherLimit);
                            break;
                        }
                    }
                }
            });
        }

        return errorMsg.toString();
    }
}
