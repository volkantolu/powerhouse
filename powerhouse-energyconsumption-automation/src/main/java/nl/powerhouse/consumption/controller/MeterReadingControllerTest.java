package nl.powerhouse.consumption.controller;

import nl.powerhouse.consumption.controller.helper.MeterReadingHelper;
import nl.powerhouse.consumption.domain.Fraction;
import nl.powerhouse.consumption.domain.MeterReading;
import nl.powerhouse.consumption.exception.*;
import nl.powerhouse.consumption.rest.client.RestClient;
import nl.powerhouse.consumption.service.MeterReadingService;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Title("This will validate meter reading for given profile")
@Features("CR 256A :This is for consumption for meter readings ")
@Stories({"User can add meter reading","User can update meter reading","User can get meter reading","User can delete meter reading"})
@Description("This is meter reading test class ")
@Controller
@RequestMapping("/consumption/meterreading/test")
public class MeterReadingControllerTest {
    @Autowired
    private MeterReadingService meterReadingService;

    @Autowired
    private MeterReadingHelper  meterReadingHelper;

    @Autowired
    public RestClient restClient;

    @RequestMapping(value = "/{profile}", method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity getMeterReadingByProfile(@PathVariable("profile") String profile) {
        try {
            return new ResponseEntity<List<MeterReading>>(meterReadingService.getMeterReadingByProfile(profile), HttpStatus.OK);
        } catch (EnergyConsumptionException e) {
            return new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity list() {
        try {
            return new ResponseEntity<List<MeterReading>>(meterReadingService.getAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }
    
    @Test
    @Title("This will validate adding meter reading for given profile")
    @Step("Assertion for meter Reading")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This is adding meter reading test method")
    @Features("CR 256A :This is for consumption for meter readings ")
    @Stories("User can add meter reading")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> addList(@RequestBody List<MeterReading> meterReadingList) {
        String errorMsg = null;
        try {
            errorMsg =  meterReadingHelper.validate(meterReadingList);
            meterReadingList.forEach(f -> {
                List<MeterReading> oldMeterReadingList = (List<MeterReading>) getMeterReadingByProfile(f.getMonthProfileKey().getProfile()).getBody();
                if (oldMeterReadingList != null && oldMeterReadingList.size() > 0)
                    throw new EnergyConsumptionException("Meter Reading for " + f.getMonthProfileKey().getProfile() + " already exists");
            });
            if (errorMsg == null || errorMsg.length() == 0) {
            	restClient.getRestCallForMeterReading("addList", "POST", meterReadingList);
                //meterReadingService.addList(meterReadingList);
                return new ResponseEntity<String>("Success", HttpStatus.OK);
            } else {
                return new ResponseEntity<String>(errorMsg, HttpStatus.OK);
            }
        } catch (EnergyConsumptionException e) {
            return new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<String> update(@RequestBody List<MeterReading> meterReadingList) {
        try {
            meterReadingHelper.validate(meterReadingList);
            meterReadingList.forEach(f -> {
                List<MeterReading> oldMeterReadingList = (List<MeterReading>) getMeterReadingByProfile(f.getMonthProfileKey().getProfile()).getBody();
                if (oldMeterReadingList == null || oldMeterReadingList.size() == 0)
                    throw new EnergyConsumptionException("Meter reading for " + f.getMonthProfileKey().getProfile() + " profile does not exist");
            });
            //TODO:
            deleteByProfile(meterReadingList.get(0).getMonthProfileKey().getProfile());
            addList(meterReadingList);
            return new ResponseEntity<String>("Success", HttpStatus.OK);
        } catch (EnergyConsumptionException e) {
            return new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/{profile}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteByProfile(@PathVariable("profile") String profile) {
        try {
            meterReadingService.deleteMeterReadingsByProfile(profile);
            return new ResponseEntity<String>("Success", HttpStatus.OK);
        } catch (EnergyConsumptionException e) {
            return new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }


}
