package nl.powerhouse.consumption.controller.helper;

import nl.powerhouse.consumption.domain.Fraction;
import nl.powerhouse.consumption.exception.*;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.stereotype.Component;

import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


@Component
public class FractionHelperImpl implements FractionHelper {


    @Override
    public void validateForTotalFraction(List<Fraction> fractionList) throws EnergyConsumptionException {

        Map<String, DoubleSummaryStatistics> map =
                fractionList.stream().collect(Collectors.groupingBy(f -> f.getMonthProfileKey().getProfile(), Collectors.summarizingDouble(Fraction::getFraction)));

        Set<String> profiles = map.keySet();

		profiles.forEach(p -> {
			Assert.assertEquals("Validation error : For profile " + p + " sum of all fractions should be 1", 1,
					map.get(p).getSum(), 0.01);
		});

    }
}
