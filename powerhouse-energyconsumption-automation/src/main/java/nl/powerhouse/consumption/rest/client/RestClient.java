package nl.powerhouse.consumption.rest.client;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import nl.powerhouse.consumption.domain.Fraction;
import nl.powerhouse.consumption.domain.MeterReading;

@Service
public class RestClient {

	public String getResponseForFraction(String url, String httpProtocol, List<Fraction> request) {
		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		
		HttpEntity<List<Fraction>> entity = new HttpEntity<List<Fraction>>(request, headers);
		
		ResponseEntity<String> result = new ResponseEntity<>(HttpStatus.HTTP_VERSION_NOT_SUPPORTED);
		
		if (httpProtocol == "POST"){
			result = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
		}
		else if (httpProtocol == "GET"){
			result = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
		}
		
		System.out.println(result);

		return result.getBody();

	}

	public String getResponseForMeterReading(String url, String httpProtocol, List<MeterReading> request) {
		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		
		HttpEntity<List<MeterReading>> entity = new HttpEntity<List<MeterReading>>(request, headers);
		
		ResponseEntity<String> result = new ResponseEntity<>(HttpStatus.HTTP_VERSION_NOT_SUPPORTED);
		
		if (httpProtocol == "POST"){
			result = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
		}
		else if (httpProtocol == "GET"){
			result = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
		}
		
		System.out.println(result);

		return result.getBody();

	}
	
	public String getRestCallForFraction(String resourceName, String httpProtocol, List<Fraction> request) {
		String url = "";
		
		if (resourceName == "addList") {
			url = "http://localhost:8181/consumption/fraction/";
		}

		return getResponseForFraction(url, httpProtocol, request);
	}
	
	public String getRestCallForMeterReading(String resourceName, String httpProtocol, List<MeterReading> request) {
		String url = "";
		
		if (resourceName == "addList") {
			url = "http://localhost:8181/consumption/meterreading/";
		}

		return getResponseForMeterReading(url, httpProtocol, request);
	}

}
