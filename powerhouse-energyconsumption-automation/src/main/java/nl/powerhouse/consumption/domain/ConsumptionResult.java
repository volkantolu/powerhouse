package nl.powerhouse.consumption.domain;

/**
 * Created by elif on 28.11.2016.
 */
public class ConsumptionResult {

    int consumption;

    public ConsumptionResult() {

    }

    public ConsumptionResult(int consumption) {
        this.consumption = consumption;
    }

    public int getConsumption() {
        return consumption;
    }

    public void setConsumption(int consumption) {
        this.consumption = consumption;
    }
}
