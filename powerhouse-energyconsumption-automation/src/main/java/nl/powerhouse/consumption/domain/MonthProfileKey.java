package nl.powerhouse.consumption.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by elif on 27.11.2016.
 */
@Embeddable
public class MonthProfileKey implements Serializable {


    private String month;

    private String profile;

    public MonthProfileKey(){
    }
    public MonthProfileKey(String month, String profile) {
        this.month = month;
        this.profile = profile;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }
}
