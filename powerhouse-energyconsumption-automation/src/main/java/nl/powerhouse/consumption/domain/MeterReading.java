package nl.powerhouse.consumption.domain;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by elif on 27.11.2016.
 */
@Entity
public class MeterReading {

    private Long connectionId;
    private Integer meterReading;

    @EmbeddedId
    private MonthProfileKey monthProfileKey;
    public MeterReading() {
    }

    public MeterReading(Long connectionId, Integer meterReading, MonthProfileKey monthProfileKey) {
        this.connectionId = connectionId;
        this.meterReading = meterReading;
        this.monthProfileKey = monthProfileKey;
    }

    public Long getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(Long connectionId) {
        this.connectionId = connectionId;
    }

    public MonthProfileKey getMonthProfileKey() {
        return monthProfileKey;
    }

    public void setMonthProfileKey(MonthProfileKey monthProfileKey) {
        this.monthProfileKey = monthProfileKey;
    }

    public Integer getMeterReading() {
        return meterReading;
    }

    public void setMeterReading(Integer meterReading) {
        this.meterReading = meterReading;
    }

}
