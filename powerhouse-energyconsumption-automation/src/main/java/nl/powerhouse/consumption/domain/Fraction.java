package nl.powerhouse.consumption.domain;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by elif on 27.11.2016.
 */
@Entity
public class Fraction {
    private Double fraction;

    @EmbeddedId
    private MonthProfileKey monthProfileKey;

    public Fraction() {
    }

    public Fraction(Double fraction, MonthProfileKey monthProfileKey) {
        this.fraction = fraction;
        this.monthProfileKey = monthProfileKey;
    }

    public MonthProfileKey getMonthProfileKey() {
        return monthProfileKey;
    }

    public void setMonthProfileKey(MonthProfileKey monthProfileKey) {
        this.monthProfileKey = monthProfileKey;
    }

    public Double getFraction() {
        return fraction;
    }

    public void setFraction(Double fraction) {
        this.fraction = fraction;
    }

}
