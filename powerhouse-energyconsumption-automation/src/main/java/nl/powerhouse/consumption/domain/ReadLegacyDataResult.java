package nl.powerhouse.consumption.domain;

/**
 * Created by elif on 28.11.2016.
 */
public class ReadLegacyDataResult {

    String result;

    public ReadLegacyDataResult() {

    }

    public ReadLegacyDataResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String consumption) {
        this.result = result;
    }
}
